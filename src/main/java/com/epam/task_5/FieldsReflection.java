package com.epam.task_5;

import java.lang.reflect.Field;

public class FieldsReflection {
    private int numb;
    private String line;

    public FieldsReflection(int numb) {
        this.numb = numb;
    }

    public FieldsReflection(int numb, String line) {
        this.numb = numb;
        this.line = line;
    }

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        FieldsReflection fields = new FieldsReflection(1_000, "Bob");
        Field field = fields.getClass().getDeclaredField("numb");
        if(field.getType().equals(int.class)){
            field.setInt(fields, 4_500);
        }
        System.out.println(fields.numb);
    }
}
