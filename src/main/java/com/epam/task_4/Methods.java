package com.epam.task_4;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Methods {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Methods methods = new Methods();
        invokeMethods(methods, 12, 4, "Hello", 5);
    }

    public int firstMethod(int a, int b) {
        return a + b;
    }

    public void secondMethod(String s) {
        System.out.println("Second: " + s.length());
    }

    public boolean thirdMethod(int a) {
        return a > 10;
    }

    private static void invokeMethods(Methods methods, int a, int b, String s, int c)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class someClass = Methods.class;
        Method firstMethod = someClass.getDeclaredMethod("firstMethod", int.class, int.class);
        System.out.println("First method: " + firstMethod.invoke(methods, a, b));
        Method secondMethod = someClass.getDeclaredMethod("secondMethod", String.class);
        secondMethod.invoke(methods, s);
        Method thirdMethod = someClass.getDeclaredMethod("thirdMethod", int.class);
        System.out.println("Third Method: " + thirdMethod.invoke(methods, c));
    }
}
