package com.epam.task_7;

import com.epam.task_5.FieldsReflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class InfoAboutClass {
    public static void main(String[] args) {
        info(FieldsReflection.class);
    }

    private static void info(Class someClass){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Full class name: " + someClass.getName() + "\n")
                .append("Class name: " + someClass.getSimpleName() + "\n")
                .append("Anonymous: "  + someClass.isAnonymousClass() + "\n")
                .append("Annotation: " + someClass.isAnnotation() + "\n")
                .append("Interface: " + someClass.isInterface() + "\n")
                .append("Primitive: " + someClass.isPrimitive() + "\n")
                .append("Enum: " + someClass.isEnum() +"\n")
                .append("Local class: " + someClass.isLocalClass() + "\n")
                .append("Array: " + someClass.isArray() + "\n")
                .append("Member Class: " + someClass.isMemberClass() + "\n");
        stringBuilder.append("\tConstructors\n");
        Constructor[] constructors = someClass.getConstructors();
        for(Constructor c : constructors){
            stringBuilder.append(c).append("\n");
        }
        stringBuilder.append("\tMethods\n");
        Method[] methods = someClass.getDeclaredMethods();
        for(Method method : methods){
            stringBuilder.append(method).append("\n");
        }
        stringBuilder.append("\tFields\n");
        Field[]fields = someClass.getDeclaredFields();
        for(Field field : fields){
            stringBuilder.append(field).append("\n");
        }
        System.out.println(stringBuilder);
    }
}
