package com.epam.task_3;

import com.epam.task_2.MyAnnotation;
import com.epam.task_2.Person;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrintAnnotation {
    public static void main(String[] args) {
        getValueOfFields(Person.class);
    }

    private static void getValueOfFields(Class someClass){
        List<Field> fields = new ArrayList<>(Arrays.asList(someClass.getDeclaredFields()));
        fields.stream()
                .filter(n -> n.isAnnotationPresent(MyAnnotation.class))
                .map(field -> field.getAnnotationsByType(MyAnnotation.class))
                .forEach(s -> System.out.println(s[0].value()));
    }
}
