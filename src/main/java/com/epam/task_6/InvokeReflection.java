package com.epam.task_6;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class InvokeReflection {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        InvokeReflection invokeReflection = new InvokeReflection();
        Method method = invokeReflection.getClass().getDeclaredMethod("myMethod", String.class, int[].class);
        method.invoke(invokeReflection, "First invoke", new int[]{10, 20, 30, 40, 50});
        method = invokeReflection.getClass().getDeclaredMethod("myMethod", String[].class);
        method.invoke(invokeReflection, new Object[]{new String[]{"Second invoke", "Reflection"}});
    }

    void myMethod(String a, int... args) {
        System.out.println("a = " + a + " args = " + Arrays.toString(args));
    }

    void myMethod(String... args) {
        System.out.println("args = " + Arrays.toString(args));
    }
}
