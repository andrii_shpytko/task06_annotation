package com.epam.task_2;

public class Person {
    @MyAnnotation(value = "name")
    private String name;
    @MyAnnotation
    private int age;
    @MyAnnotation
    private int phone;
    @MyAnnotation(value = "Male / Female")
    private char sex;

    public Person(String name, int age, int phone, char sex) {
        this.name = name;
        this.age = age;
        this.phone = phone;
        this.sex = sex;
    }
}
