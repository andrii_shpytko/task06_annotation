package com.epam.task_2;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MyTest {
    public static void main(String[] args) {
        MyTest test = new MyTest();
        test.getAnnotatedField(Person.class);
    }

    void getAnnotatedField(Class someClass) {
        List<Field> fields = new ArrayList<>(Arrays.asList(someClass.getDeclaredFields()));
        fields.stream()
                .filter(n -> n.isAnnotationPresent(MyAnnotation.class))
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }
}
